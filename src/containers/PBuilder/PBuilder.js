import React , {Component} from "react";
import Auxi from "../../HOC/Auxi";
import Jeu from "../../composants/jeu/jeu";
import Modal from "../../composants/modal/modal"
import InfoJeu from "../../composants/infoJeu/infoJeu"
import Joueurs from "../../composants/jeu/joueurs/joueur"
import Controleurs from '../../composants/controleurs/controleurs'
import classe from "./PBuilder.scss"


class PBuilder extends Component {
    state= {
        tableaux: [
            [0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0],
        ],

        modal:false,

        joueur1: 1,
        joueur2: 2,
        joueurActif: 1,


        joueurs:{
            nomJoueur1 : '',
            nomJoueur2 : '',
        }

    }
// met à jour les states des noms des joueurs
    componentDidMount(){
        this.setState({
            joueurs:{
                nomJoueur1: this.props.location.state.joueur1,
                nomJoueur2: this.props.location.state.joueur2,
            },
        });
    }
   // Permet de changer de joueur à chaque tours
    switchJoueursMemoire() {
        return (this.state.joueurActif === this.state.joueur1) ? this.state.joueur2 : this.state.joueur1;
    }

    //Permet de changer le state du joueur actif et de la grille
    switchJoueurs=(label,i)=>{
        let indice= this.lastElement(label,i)
        //on sauvegarde la valeur à changer
        const ancienneValeur = this.state.tableaux[indice][label]

        let nouvelleValeur = 0;

        if(this.state.joueurActif===1){
            nouvelleValeur=ancienneValeur+1
        }
        if(this.state.joueurActif===2){
            nouvelleValeur=ancienneValeur+2
        }
        //on initialise une nouvelle variable pour pouvoir mettre à jour le tableaux et pour le faire on récupère tout le tableaux
        const MajTableaux = {...this.state.tableaux}
        MajTableaux[indice][label]=nouvelleValeur

        //On met à jour le nouveaux tableaux,son state
        this.setState({
            tableaux:MajTableaux
        });
        this.setState({
            joueurActif: this.switchJoueursMemoire()
        })


        let resultat= this.verifTotal() || this.verifEgalite()
        if( resultat === this.state.joueur1){
            this.setState({modal:true});
        }
        if( resultat === this.state.joueur2){
            this.setState({modal:true});
        }
        if( resultat === 'egalite'){
            this.setState({modal:true,joueurActif:0})
        }
    }

    //Determine le dernier élément libre dans chaque colonne ( en partant du bas du tableaux)
    lastElement=(label)=>{
        let Elemvide= null
        for(let i=5;i>=0;i--){
            if (this.state.tableaux[i][label]===0){
                Elemvide= this.state.tableaux[i][label];
                return i;
            }
        }
    }

    /// Conditions de victoire

    verifVerticale = ()=>{
        //i = ligne horizontale
        for (let i = 3; i < 6; i++) {
            //j=>ligne verticales
            for (let j = 0; j < 7; j++) {
                if (this.state.tableaux[i][j]) {
                    if (this.state.tableaux[i][j] === this.state.tableaux[i - 1][j] &&
                        this.state.tableaux[i][j] === this.state.tableaux[i - 2][j] &&
                        this.state.tableaux[i][j] === this.state.tableaux[i - 3][j]) {
                        return this.state.tableaux[i][j];
                    }
                }
            }
        }
    }

    verifHorizontal() {

        for (let i = 0; i < 6; i++) {
            for (let j = 0; j < 4; j++) {
                if (this.state.tableaux[i][j]) {
                    if (this.state.tableaux[i][j] === this.state.tableaux[i][j + 1] &&
                        this.state.tableaux[i][j] === this.state.tableaux[i][j + 2] &&
                        this.state.tableaux[i][j] === this.state.tableaux[i][j + 3]) {
                        return this.state.tableaux[i][j];
                    }
                }
            }
        }
    }

    verifDiagonaleDroite() {

        for (let i = 3; i < 6; i++) {
            for (let j = 0; j < 4; j++) {
                if (this.state.tableaux[i][j]) {
                    if (this.state.tableaux[i][j] === this.state.tableaux[i - 1][j + 1] &&
                        this.state.tableaux[i][j] === this.state.tableaux[i - 2][j + 2] &&
                        this.state.tableaux[i][j] === this.state.tableaux[i - 3][j + 3]) {
                        return this.state.tableaux[i][j];
                    }
                }
            }
        }
    }

    verifDiagonaleGauche() {

        for (let i = 3; i < 6; i++) {
            for (let j = 3; j < 7; j++) {
                if (this.state.tableaux[i][j]){
                    if (this.state.tableaux[i][j] === this.state.tableaux[i - 1][j - 1] &&
                        this.state.tableaux[i][j] === this.state.tableaux[i - 2][j - 2] &&
                        this.state.tableaux[i][j] === this.state.tableaux[i - 3][j - 3]) {
                        return this.state.tableaux[i][j];
                    }
                }
            }
        }
    }

    verifEgalite() {
        for (let i = 0; i < 6; i++) {
            for (let j = 0; j < 7; j++) {
                if (this.state.tableaux[i][j] === 0) {
                    return null;
                }
            }
        }
        return 'egalite';
    }
// vérifie qu'une des conditions de victoire est remplie
    verifTotal =()=>{
        return this.verifVerticale() || this.verifHorizontal() || this.verifDiagonaleDroite() || this.verifDiagonaleGauche() || this.verifEgalite()
    }

    // rajoute un bouton dans le modal pour relancer une partie en rechargeant la page
    rejouer =()=>{
        //eslint-disable-next-line
        location.reload(true)
    }

    // rajoute un bouton pour remettre la partie à zéros en rechargeant la page
    reset =()=>{
        //eslint-disable-next-line
        location.reload(true)
    }

    //rajoute un bouton pour retourner sur la pages d'entr&ée des logins en cas d'erreur
    retourLogin=()=>{
        document.location='/'
    }

    render (){

        // S'assure que le bouton est désactivé quand la colonne est totalement remplie
        const desactivationBouton = {
            ...this.state.tableaux[0]
        }
        for(let key in desactivationBouton){
            desactivationBouton[key] = desactivationBouton[key] <=0
        }
        return (

            <Auxi>
                <h1>Bienvenue sur le jeu du puissance 4.</h1>
                <Modal modal={this.state.modal}>
                    <InfoJeu joueurActif={this.state.joueurActif} joueur1={this.state.joueurs.nomJoueur1} joueur2={this.state.joueurs.nomJoueur2} rejouer={this.rejouer} />
                </Modal>
                <Controleurs switchJoueurs={this.switchJoueurs} switchActif={desactivationBouton}/>
                <div className={classe.position}><Jeu tableaux={this.state.tableaux} ajoutJeton={this.ajoutJeton} />
                <Joueurs nom={this.state.joueurs.nomJoueur1} nom2={this.state.joueurs.nomJoueur2} joueurActif={this.state.joueurActif} reset={this.reset} retourLogin={this.retourLogin}/></div>
            </Auxi>
        )
    }

}

export default PBuilder;