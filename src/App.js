import React, { Component } from 'react';
import Template from './composants/Template/Template';
import PBuilder from "./containers/PBuilder/PBuilder";
import Connection from "./composants/connection/Connection"
import {BrowserRouter} from 'react-router-dom';
import {Route,Switch} from "react-router-dom"



class App extends Component {
  render() {
    return (
        <BrowserRouter>
      <div >
        <Template>
             <Switch>
            <Route exact path ="/"  component={Connection}/>
            <Route exact path="/jeu" component={PBuilder}/>
             </Switch>
        </Template>
      </div>
        </BrowserRouter>
    );
  }
}

export default App;
