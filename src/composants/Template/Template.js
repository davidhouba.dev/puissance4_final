import React from "react";
import Auxi from "../../HOC/Auxi";
import classe from "./Template.scss"


const template = (props) => (
    <Auxi>
    <main className={classe.main}>
    {props.children}
    </main>

    </Auxi>
)

export default template;