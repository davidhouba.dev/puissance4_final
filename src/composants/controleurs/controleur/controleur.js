import React from 'react'
import Class from './controleur.scss'

const controleur = (props) =>(

    <div className={Class.controleur}>
        <button onClick={props.switchJoueurs} disabled={!props.switchActif}>Jouez ici</button>
    </div>
)

export default controleur