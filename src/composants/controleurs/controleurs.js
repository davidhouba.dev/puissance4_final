import React from 'react'
import Controleur from './controleur/controleur'
import classe from './controleurs.scss'

const controls = [
    {
        label:'0',type:'colonne1'
    },
    {
        label:'1',type:'colonne2'
    },
    {
        label:'2',type:'colonne3'
    },
    {
        label:'3',type:'colonne4'
    },
    {
        label:'4',type:'colonne5'
    },
    {
        label:'5',type:'colonne6'
    },
    {
        label:'6',type:'colonne7'
    },
]

const controleurs = (props) => (

    <div className={classe.boutonsJouer}>
        {controls.map(ctrl=>(
                <Controleur switchJoueurs={()=>props.switchJoueurs(ctrl.label)} key={ctrl.label} label={ctrl.type} switchActif={props.switchActif[ctrl.label]}/>
            ))}
    </div>

);

export default controleurs