import React from 'react'
import classes from './modal.scss'


const modal = (props) => {
    if (props.modal=== true){
       return (
           <div className={classes.modal}>
               {props.children}
           </div>
       )
    }else {
        return null
    }
};

export default modal