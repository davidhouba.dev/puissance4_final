import React ,{Component} from "react"
import classes from "./connection.scss"

class Connection extends Component{
    state = {
        joueurs:{
            nomJoueur1 : '',
            nomJoueur2 : '',
        }
    };

    //met à jour les noms de joueurs
    MajJoueur = (event) => {
        this.setState ({
            [event.target.name] : event.target.value,
        })
    };

    //envoie les noms sur la page de jeu
    next = (e) => {
        e.preventDefault();
        this.props.history.push({
            pathname: '/jeu',
            state: {
                joueur1: this.state.nomJoueur1,
                joueur2: this.state.nomJoueur2,
            }
        });
    };

render (){
        return(
            <form>
                <label className={classes["titre-principal"]}>Bienvenue dans le jeu du Puissance 4 </label>
                    <label className={classes["sous-titre"]}>Entrez les noms des joueurs</label>
                        <div className={classes.interfaceCo}>
                         <label className={classes.titreInput}>Joueur 1 :</label>
                             <input type="text" name="nomJoueur1" onChange={this.MajJoueur}  required />
                         <label className={classes.titreInput}>Joueur 2 :</label>
                             <input type="text" name="nomJoueur2" onChange={this.MajJoueur} required/></div>
                  <button className={classes.boutonconnection} type="submit"  onClick={this.next}> Jouer !</button>
            </form>
        )
    }
};

export default Connection


