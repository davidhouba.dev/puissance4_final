import React from "react";
import classe from "./jeu.scss"
import Colonnes from "./colonnes/colonnes"

const jeu = (props) => {
    const tableaux =Object.keys(props.tableaux[0])
        .map((__,i) => {
            return (
                <div key={i}>
                    <Colonnes key={i} tableaux={props.tableaux} indexColonne={i}></Colonnes>
                </div>
            )
        })
    return(
        <div className={classe.jeu}>{tableaux}</div>
    )
}


export default jeu;
