import React from 'react'
import classe from './joueurs.scss'



const joueurs = (props) => {

if(props.joueurActif === 1){
    return(
        <div className={classe.center} >
        <p>
            C'est au tour de <span>{props.nom}</span> de jouer :
        </p>
            <p className={classe.joueur1}></p>
            <p><button onClick={props.reset}>Reset</button></p>
            <p><button onClick={props.retourLogin}>Changer de pseudos</button></p>
        </div>
    )
}else if (props.joueurActif === 2){
    return(
        <div className={classe.center}>
            <p>
                C'est au tour de <span>{props.nom2}</span> de jouer :
            </p>
            <p className={classe.joueur2}></p>
            <p><button onClick={props.reset}>Reset</button></p>
            <p><button onClick={props.retourLogin}>Changer de pseudos</button></p>
        </div>
    )

}


return(
    <div>
    <p>{props.nom}</p>
    </div>


)
}

export default joueurs
