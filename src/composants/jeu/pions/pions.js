import React ,{Component} from 'react'
import classes from './pions.scss'

class pions extends Component {

    render () {
        let pionColor;
        if (this.props.coordonnees === 0) {
            pionColor = ''
        } else if (this.props.coordonnees === 1) {
            pionColor = classes.joueur1
        } else if (this.props.coordonnees === 2) {
            pionColor = classes.joueur2
        } else {
            pionColor = ''
        }

        return (
            <div className={pionColor} />
        )
    }

}

export default pions