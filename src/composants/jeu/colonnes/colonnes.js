import React from "react";
import classes from "./colonnes.scss"
import Case from "../case/case"


const colonnes = (props) => {
    const cases = Object.keys(props.tableaux)
        .map((__,j) => {
            return <Case key={j} indexColonne={props.indexColonne} indexCase={j} tableaux={props.tableaux}></Case>
        })

    return(
        <div className={classes.jeu}>{cases}</div>
    )
}

export default colonnes;
