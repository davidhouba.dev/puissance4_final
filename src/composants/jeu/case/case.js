import React from 'react'
import classes from './case.scss'
import Pion from '../pions/pions'

const Case = (props)=> {

        const Coordonnees = props.tableaux[props.indexCase][props.indexColonne]
        return (
            <div className={classes.case}>
                <Pion coordonnees={Coordonnees}/>
            </div>

        )

}
export default Case