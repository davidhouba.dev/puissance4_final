import React from 'react'
import Classes from './infoJeu.scss'
import Auxi from "../../HOC/Auxi";

// Définit le contenu du message affiché par le modal

const infoJeu = (props) => {

    if(props.joueurActif===1){
        return (
            <Auxi>
                <p className={Classes.infoJeu}> <span className={Classes.span}>{props.joueur2}</span> <span className={Classes.textColor}>à gagné</span></p>
                <p className={Classes.infoJeu}><button onClick={props.rejouer}>rejouer ?</button></p>
            </Auxi>
        )
    }
    if (props.joueurActif ===2){
        return (
            <Auxi>
                <p className={Classes.infoJeu}> <span className={Classes.span}>{props.joueur1}</span> <span className={Classes.textColor}>à gagné</span></p>
                <p className={Classes.infoJeu}><button onClick={props.rejouer}>rejouer ?</button></p>
            </Auxi>
        )
    }

    if (props.joueurActif ===0){
        return (
            <Auxi>
                <p className={Classes.infoJeu}> <span className={Classes.null}>Match Nul !!</span></p>
                <p className={Classes.infoJeu}><button onClick={props.rejouer}>rejouer ?</button></p>
            </Auxi>
        )
    }

};



export default infoJeu

